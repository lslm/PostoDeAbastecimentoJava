package postodeabastecimento;

public class Funcionario {
    private String numero;
    private String nome;
    
    public boolean equals(Object outroObjeto) {
        Funcionario outroFuncionario = (Funcionario) outroObjeto;
        return this.numero == outroFuncionario.getNumero();
    }
    
    public Funcionario(String numero, String nome) {
        this.numero = numero;
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public String getNome() {
        return nome;
    }
}
