package postodeabastecimento;

import java.util.Date;

public class Consumo {
    private Bomba bomba;
    private Funcionario funcionario;
    private EnumTiposCombustivel tipoCombustivel;
    private Date data;
    private double litros;
    
    public Consumo(Bomba bomba,
                     Funcionario funcionario,
                     EnumTiposCombustivel tipoCombustivel,
                     double litros,
                     Date data)
    {
        this.bomba = bomba;
        this.funcionario = funcionario;
        this.tipoCombustivel = tipoCombustivel;
        this.setLitros(litros);
        this.data = data;
    }

    public Bomba getBomba() {
        return bomba;
    }

    public void setLitros(double litros) {
        if (tipoCombustivel == EnumTiposCombustivel.ALCOOL) {
            bomba.atualizarTotalAlcool(litros);
        } else if (tipoCombustivel == EnumTiposCombustivel.DIESEL) {
            bomba.atualizarTotalDiesel(litros);
        } else {
            bomba.atualizarTotalGasolina(litros);
        }
        
        this.litros = litros;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public double getLitros() {
        return litros;
    }
    
    public Date getData() {
        return data;
    }

    public EnumTiposCombustivel getTipoCombustivel() {
        return tipoCombustivel;
    }
}
