package postodeabastecimento;

public class Bomba {
    private String numero;
    private boolean aceitaAlcool;
    private boolean aceitaGasolina;
    private boolean aceitaDiesel;
    private double totalAlcool;
    private double totalDiesel;
    private double totalGasolina;
    
    public Bomba(String numero,
                   boolean aceitaAlcool,
                   boolean aceitaGasolina,
                   boolean aceitaDiesel)
    {
        this.numero = numero;
        this.aceitaAlcool = aceitaAlcool;
        this.aceitaGasolina = aceitaGasolina;
        this.aceitaDiesel = aceitaDiesel;
        this.totalAlcool = 0;
        this.totalDiesel = 0;
        this.totalGasolina = 0;
    }
    
    public boolean equals(Object outroObjeto) {
        Bomba outraBomba = (Bomba) outroObjeto;
        return this.numero == outraBomba.getNumero();
    }
    
    public String getResumo() {
        String resumo = "Numero: " + this.numero;
        if(this.aceitaAlcool)
            resumo += "\nTotal álcool: " + this.getTotalAlcool() + " litros";
        if (this.aceitaGasolina)
            resumo += "\nTotal gasolina: " + this.getTotalGasolina() + " litros";
        if(this.aceitaDiesel)
            resumo += "\nTotal diesel: " + this.getTotalDiesel() + " litros";
        
        return resumo;
    }

    public double getTotalAlcool() {
        return totalAlcool;
    }

    public double getTotalDiesel() {
        return totalDiesel;
    }

    public double getTotalGasolina() {
        return totalGasolina;
    }
    
    
    public void atualizarTotalAlcool(double ltAlcool) {
        this.totalAlcool += ltAlcool;
        TotaisCombustivel.atualizarTotalAlcool(ltAlcool);
    }
    
    public void atualizarTotalGasolina(double ltGasolina) {
        this.totalGasolina += ltGasolina;
        TotaisCombustivel.atualizarTotalGasolina(ltGasolina);
    }
    
    public void atualizarTotalDiesel(double ltDiesel) {
        this.totalDiesel += ltDiesel;
        TotaisCombustivel.atualizarTotalDiesel(ltDiesel);
    }    
    
    public String getNumero() {
        return this.numero;
    }
    
    public boolean getAceitaAlcool() {
        return this.aceitaAlcool;
    }
    
    public boolean getAceitaGasolina() {
        return this.aceitaGasolina;
    }
    
    public boolean getAceitaDiesel() {
        return this.aceitaDiesel;
    }
}
