Menu

 - Cadastrar bomba

 |_ - Numero da bomba

 |_ - Aceita �lcool

 |_ - Aceita gasolina

 |_ - Aceita diesel

 - Cadastrar funcionario

 |_ - Numero

 |_ - Nome

 - Adicionar consumo

 |_ - Qual o combustivel? (1 - Alcool, 2 - Gasolina, 3 - Diesel)

 |_ - Quantos litros?

 |_ - Numero do funcionario

 - Exibir resumo de vendas no posto

 - Exibir resumo de vendas por bomba

 |_ - Numero da bomba

 - Exibir combustivel mais vendido no posto

 - Exibir listagem de vendas

 - Sair


--------------------------------------------------------

Exemplos de relat�rios

____________________________

4. Resumo de vendas no posto|

 - 141 litros de alcool     |

 - 200 litros de gasolina   |

 - 1020 litros de diesel    |

____________________________|


_____________________________

5. Resumo de vendas por bomba|

 - 141 litros de alcool      |

 - 200 litros de gasolina    |

 - 1020 litros de diesel     |

_____________________________|


___________________________

6. Combustivel mais vendido|

 - Diesel (1020 litros)    |

___________________________|


_________________________________________________________

7. Listagem de vendas                                    |

23/08/2016 08:53 - Diesel - 22 litros - Jos� - Bomba 1   |

23/08/2016 09:10 - Gasolina - 22 litros - Paulo - Bomba 2|

_________________________________________________________|