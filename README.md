Um posto de combustível tem várias bombas,
que podem ter álcool e/ou gasolina e/ou diesel.

Você foi cotratado para desenvolver um sistema que

recebe das bombas os seguintes dados:

 - Data de abastecimento

 - Hora de abastecimento

 - Quantidade de litros

 - Tipo de combustivel

 - Funcionário que abasteceu



O sistema deve exibir os seguintes relatórios:

 - Quanto de combustível que cada bomba vendeu

 - qual foi o combustível mais vendido do posto

 - Listagem de vendas total do posto

 - Quantidade total de litros vendidos no posto
 para cada tipo de combustível



Como não há bombas automáticas para testar, crie

uma interface no sistema para informar os dados

